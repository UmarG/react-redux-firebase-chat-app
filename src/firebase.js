import firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyAF3rW3ioZ42wZUc6h8-r7cVzqjJB15Ats',
  authDomain: 'react-redux-firebase-chat-app.firebaseapp.com',
  databaseURL: 'https://react-redux-firebase-chat-app.firebaseio.com',
  projectId: 'react-redux-firebase-chat-app',
  storageBucket: 'react-redux-firebase-chat-app.appspot.com',
  messagingSenderId: '575298662864'
};

firebase.initializeApp(config);

export default firebase;

export const database = firebase.database();
export const auth = firebase.auth();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
export const messaging = firebase.messaging();
